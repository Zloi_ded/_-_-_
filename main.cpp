#include <iostream>
#include <locale.h>
#include <fstream>
#include <cstring>
#include <string>
#include <stdlib.h>


const int N = 150;
const int N2 = 50;


class Computer_game
{
    protected:
        char name_of_game[N];
        char developer[N];
        int release_date_day, release_date_month, release_date_year;
        float critical_evaluation;
        char game_mode[N];

    public:
        Computer_game()
        {
            strcpy (name_of_game," ");
            strcpy (developer," ");
            release_date_day = 0;
            release_date_month = 0;
            release_date_year = 0;
            critical_evaluation = 0;
            strcpy (game_mode," ");
        }

        Computer_game(char c1[N], char c2[N], int i1, int i2, int i3, float f, char c3[N])
        {
            strcpy (name_of_game,c1);
            strcpy (developer,c2);
            release_date_day = i1;
            release_date_month = i2;
            release_date_year = i3;
            critical_evaluation = f;
            strcpy (game_mode,c3);
        }
};


class RPG : public Computer_game
{
    protected:
        char specifications_of_characters[N];
        char basic_of_gameplay[N2]; // "���������� ������� ���������"

    public:
        RPG (): Computer_game()
        {
            strcpy (specifications_of_characters," ");
            strcpy (basic_of_gameplay," ");
        }

        RPG (char c1[N], char c2[N], int i1, int i2,int i3, float f, char c3[N],
        char c4[N], char c5[N2]): Computer_game(c1, c2, i1, i2, i3, f, c3)
        {
            strcpy (specifications_of_characters,c4);
            strcpy (basic_of_gameplay,c5);
        }
};


class RPG_narrative : public RPG
{
    protected:
        char basic_of_game[N2]; // "�����";

    public:
        RPG_narrative (): RPG()
        {
            strcpy (basic_of_game," ");
        }

        RPG_narrative (char c1[N], char c2[N], int i1, int i2,int i3, float f, char c3[N],
        char c4[N], char c5[N2], char c6[N]) : RPG(c1, c2, i1, i2, i3, f, c3, c4, c5)
        {
            strcpy (basic_of_game,c6);
        }

        void record(const char* name_file)
        {
            std::ofstream fout(name_file);
            fout
            << name_of_game << " "
            << developer << " "
            << release_date_day << " " << release_date_month << " " << release_date_year << " "
            << " " << critical_evaluation << " "
            << game_mode << " " << specifications_of_characters << " "
            << basic_of_gameplay << " " << basic_of_game << std::endl;
            fout.close();
        }

        void reading(const char* name_file)
        {
            std::ifstream fin(name_file);
            fin
            >> name_of_game >> developer
            >> release_date_day >> release_date_month >> release_date_year
            >> critical_evaluation >> game_mode >> specifications_of_characters
            >> basic_of_gameplay >> basic_of_game;
            fin.close();
        }

        void binary_record(const char* name_file)
        {
            std::ofstream out(name_file, std::ios::binary|std::ios::out);
            out.write((char*)&name_of_game, sizeof name_of_game);
            out.write((char*)&developer, sizeof developer);
            out.write((char*)&release_date_day, sizeof release_date_day);
            out.write((char*)&release_date_month, sizeof release_date_month);
            out.write((char*)&release_date_year, sizeof release_date_year);
            out.write((char*)&critical_evaluation, sizeof critical_evaluation);
            out.write((char*)&game_mode, sizeof game_mode);

            out.write((char*)&specifications_of_characters, sizeof specifications_of_characters);
            out.write((char*)&basic_of_gameplay, sizeof basic_of_gameplay);

            out.write((char*)&basic_of_game, sizeof basic_of_game);
            out.close();
        }

        void binary_reading(const char* name_file)
        {
            std::fstream in(name_file,std::ios::binary|std::ios::in);
            in.read((char*)&name_of_game, sizeof name_of_game);
            in.read((char*)&developer, sizeof developer);
            in.read((char*)&release_date_day, sizeof release_date_day);
            in.read((char*)&release_date_month, sizeof release_date_month);
            in.read((char*)&release_date_year, sizeof release_date_year);
            in.read((char*)&critical_evaluation, sizeof critical_evaluation);
            in.read((char*)&game_mode, sizeof game_mode);

            in.read((char*)&specifications_of_characters, sizeof specifications_of_characters);
            in.read((char*)&basic_of_gameplay, sizeof basic_of_gameplay);

            in.read((char*)&basic_of_game, sizeof basic_of_game);
            in.close();
        }

        void print_RPG_N()
        {
            std::cout << "�������� ����: " <<  name_of_game << std::endl;
            std::cout << "   �����������: " <<  developer << "." << std::endl;
            std::cout << "   ���� �������: " <<  release_date_day << "." <<  release_date_month << "." <<  release_date_year << std::endl;
            std::cout << "   ������ �� Metacritic: " <<  critical_evaluation << "." << std::endl;
            std::cout << "   ����� ����: " <<  game_mode << "." << std::endl;

            std::cout << "   ������ ��� ������ ����������: " <<  specifications_of_characters << "." << std::endl;
            std::cout << "   �������������� � �����: " <<  basic_of_gameplay << "." << std::endl;

            std::cout << "   ������ ����: " <<  basic_of_game << "." << std::endl;
            std::cout << "\n";
        }
};


class RPG_with_open_world : public RPG
{
    protected:
        char basic_of_game[N2]; //"�������� ���"

    public:
        RPG_with_open_world (): RPG()
        {
            strcpy (basic_of_game," ");
        }

        RPG_with_open_world (char c1[N], char c2[N], int i1, int i2,int i3, float f, char c3[N],
        char c4[N], char c5[N2], char c7[N]) : RPG(c1, c2, i1, i2, i3, f, c3, c4, c5)
        {
            strcpy (basic_of_game,c7);
        }

        void print_RPG_W()
        {
            std::cout << "�������� ����: " <<  name_of_game << std::endl;
            std::cout << "   �����������: " <<  developer << "." << std::endl;
            std::cout << "   ���� �������: " <<  release_date_day << "." <<  release_date_month << "." <<  release_date_year << std::endl;
            std::cout << "   ������ �� Metacritic: " <<  critical_evaluation << "." << std::endl;
            std::cout << "   ����� ����: " <<  game_mode << "." << std::endl;

            std::cout << "   ������ ��� ������ ����������: " <<  specifications_of_characters << "." << std::endl;
            std::cout << "   �������������� � �����: " <<  basic_of_gameplay << "." << std::endl;

            std::cout << "   ������ ����: " <<  basic_of_game << "." << std::endl;
            std::cout << "\n";
        }
};


class Strategy : public Computer_game
{
    protected:
        char units[N];
        char basic_of_gameplay[N]; // "������������� ����������� ��������";
        char races[N];

    public:
        Strategy (): Computer_game()
        {
            strcpy (units," ");
            strcpy (basic_of_gameplay," ");
            strcpy (races," ");
        }

        Strategy(char c1[N], char c2[N], int i1, int i2,int i3, float f, char c3[N], char c8[N],
        char c9[N], char c10[N]): Computer_game(c1, c2, i1, i2, i3, f, c3)
        {
            strcpy (units,c8);
            strcpy (basic_of_gameplay,c9);
            strcpy (races,c10);
        }

};

class Strategy_TBS : public Strategy
{
    protected:
        char moves[N2]; //"���������� ����"

    public:
        Strategy_TBS (): Strategy()
        {
            strcpy (moves," ");
        }

        Strategy_TBS(char c1[N], char c2[N], int i1, int i2,int i3, float f, char c3[N], char c8[N], char c9[N],
        char c10[N], char c11[N]): Strategy(c1, c2, i1, i2, i3, f, c3, c8, c9, c10)
        {
            strcpy (moves,c11);
        }

        void print_S_TBS()
        {
            std::cout << "�������� ����: " <<  name_of_game << std::endl;
            std::cout << "   �����������: " <<  developer << "." << std::endl;
            std::cout << "   ���� �������: " <<  release_date_day << "." <<  release_date_month << "." <<  release_date_year << std::endl;
            std::cout << "   ������ �� Metacritic: " <<  critical_evaluation << "." << std::endl;
            std::cout << "   ����� ����: " <<  game_mode << "." << std::endl;

            std::cout << "   �����: " <<  units << "." << std::endl;
            std::cout << "   �������������� � �����: " <<  basic_of_gameplay << "." << std::endl;
            std::cout << "   �����(��� �������): " <<  races << "." << std::endl;

            std::cout << "   ����: " <<  moves << "." << std::endl;
        }
};


class Strategy_RTS : public Strategy
{
    protected:
        char moves[N2]; //"���� � �������� �������";

    public:
        Strategy_RTS (): Strategy()
        {
            strcpy (moves," ");
        }

        Strategy_RTS(char c1[N], char c2[N], int i1, int i2,int i3, float f, char c3[N], char c8[N],char c9[N],
        char c10[N], char c12[N]): Strategy(c1, c2, i1, i2, i3, f, c3, c8, c9, c10)
        {
            strcpy (moves,c12);
        }

        void print_S_RTS()
        {
            std::cout << "�������� ����: " <<  name_of_game << std::endl;
            std::cout << "   �����������: " <<  developer << "." << std::endl;
            std::cout << "   ���� �������: " <<  release_date_day << "." <<  release_date_month << "." <<  release_date_year << std::endl;
            std::cout << "   ������ �� Metacritic: " <<  critical_evaluation << "." << std::endl;
            std::cout << "   ����� ����: " <<  game_mode << "." << std::endl;

            std::cout << "   �����: " <<  units << "." << std::endl;
            std::cout << "   �������������� � �����: " <<  basic_of_gameplay << "." << std::endl;
            std::cout << "   �����(��� �������): " <<  races << "." << std::endl;

            std::cout << "   ����: " <<  moves << "." << std::endl;
        }
};


int main()
{
    setlocale(LC_ALL, "Rus");

    RPG_narrative Example1("Example1", "CD_Project_Red", 24, 9, 2007, 8.6, "��������������������", "!!!!", "!!!", "�������� ���");
    Example1.binary_record("Example1.txt");

    RPG_narrative  Example2;
    Example2.binary_reading("Example1.txt");
    Example2.print_RPG_N();


    RPG_with_open_world The_Witcher_3("The_Witcher_3", "CD_Project_Red", 19, 5, 2015, 9.1, "��������������������", "����������, �����, �����", "���������� ������� ���������", "�������� ���");
    The_Witcher_3.print_RPG_W();

    RPG_narrative Mass_effect("Mass effect", "Bioware", 2, 9, 2007, 9.1, "��������������������", "������, �������, �������, ����", "���������� ������� ���������", "�����");
    Mass_effect.print_RPG_N();

    Strategy_RTS Example3;
    Example3.print_S_RTS();

    Strategy_TBS Example4;
    Example4.print_S_TBS();

    return 0;
}
